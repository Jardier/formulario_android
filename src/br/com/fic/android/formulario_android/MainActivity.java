package br.com.fic.android.formulario_android;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import br.com.fic.android.formulario_android.model.Pessoa;

public class MainActivity extends Activity {

	private Spinner spinnerRaca;
	private Pessoa pessoa;
	private EditText editTextNome, editTextTelefone;
	private CheckBox checkBox1, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6, checkBox7;
	private RadioButton radioButtonClicado;
	private ListView listView;
	private ArrayList<Pessoa> listaDePessoas;
	private RadioGroup mFirstGroup, mSecondGroup;

	private boolean isChecking = true;
	private int mCheckedId = R.id.radio_tipo1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editTextNome = (EditText) findViewById(R.id.txtNome);
		editTextTelefone = (EditText) findViewById(R.id.txtTelefone);
		checkBox1 = (CheckBox) findViewById(R.id.check_box_esporte_ciclismo);
		checkBox2 = (CheckBox) findViewById(R.id.check_box_esporte_basquete);
		checkBox3 = (CheckBox) findViewById(R.id.check_box_esporte_natacao);
		checkBox4 = (CheckBox) findViewById(R.id.check_box_esporte_futebol);
		checkBox5 = (CheckBox) findViewById(R.id.check_box_esporte_box);
		checkBox6 = (CheckBox) findViewById(R.id.check_box_esporte_jodo);
		checkBox7 = (CheckBox) findViewById(R.id.check_box_esporte_mma);

		// Maneira bizarra pra resolver os Dois RadioGroup
		mFirstGroup = (RadioGroup) findViewById(R.id.first_group);
		mSecondGroup = (RadioGroup) findViewById(R.id.second_group);

		mFirstGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId != -1 && isChecking) {
					isChecking = false;
					mSecondGroup.clearCheck();
					mCheckedId = checkedId;
				}
				isChecking = true;
			}
		});

		mSecondGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId != -1 && isChecking) {
					isChecking = false;
					mFirstGroup.clearCheck();
					mCheckedId = checkedId;
				}
				isChecking = true;
			}
		});

		pessoa = new Pessoa();

		// Inicializando o spinnerRaca
		spinnerRaca = (Spinner) findViewById(R.id.spinner_raca);
		ArrayAdapter<CharSequence> spinnerRacaAdapter = ArrayAdapter.createFromResource(this, R.array.raca_array, android.R.layout.simple_spinner_dropdown_item);
		spinnerRaca.setAdapter(spinnerRacaAdapter);

		// Inciando a lista de pessoas
		listView = (ListView) findViewById(R.id.listaDePessoas);
		listaDePessoas = new ArrayList<Pessoa>();

		final MinhaListaAdapter adapter = new MinhaListaAdapter(this, listaDePessoas);
		listView.setAdapter(adapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onCkeckBoxClicked(View view) {
		CheckBox checkBoxClicado = (CheckBox) view;
		String esporte = checkBoxClicado.getText().toString();

		if (checkBoxClicado.isChecked()) {
			pessoa.addEsportesFavoritos(esporte);
		} else {
			pessoa.removeEsportesFavoritos(esporte);
		}

		System.out.println(pessoa.getEsportesFavoritos().toString());
	}

	public void onRadioButtonClicked(View view) {
		radioButtonClicado = (RadioButton) view;
		String tipoSanguineo = radioButtonClicado.getText().toString();

		if (radioButtonClicado.isClickable()) {
			pessoa.setTipoSanguineo(tipoSanguineo);
		}

	}

	public void salvarDados(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle("GRAVA��O DE DADOS");
		builder.setIcon(R.drawable.gravar);

		builder.setMessage(R.string.dlgDesejaSalvar).setPositiveButton(R.string.dlgSim, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				gravarDados();

			}
		}).setNegativeButton(R.string.dlgNao, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// executa se cancelar

			}
		});

		builder.create().show();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		final MinhaListaAdapter adapter = new MinhaListaAdapter(this, listaDePessoas);
		listView.setAdapter(adapter);
		super.onConfigurationChanged(newConfig);
	}

	private void gravarDados() {
		pessoa.setNome(editTextNome.getText().toString());
		pessoa.setTelefone(editTextTelefone.getText().toString());
		pessoa.setRaca(spinnerRaca.getSelectedItem().toString());

		listaDePessoas.add(pessoa);
		System.out.println(pessoa.getEsportes());
		limparDados();
		pessoa = new Pessoa();

		final MinhaListaAdapter adapter = new MinhaListaAdapter(this, listaDePessoas);
		listView.setAdapter(adapter);

	}

	private void limparDados() {
		checkBox1.setChecked(false);
		checkBox2.setChecked(false);
		checkBox3.setChecked(false);
		checkBox4.setChecked(false);
		checkBox5.setChecked(false);
		checkBox6.setChecked(false);
		checkBox7.setChecked(false);
		mFirstGroup.clearCheck();
		mSecondGroup.clearCheck();
		spinnerRaca.setSelection(0);
		editTextNome.setText("");
		editTextTelefone.setText("");
		editTextNome.requestFocus(0);
	}

}
