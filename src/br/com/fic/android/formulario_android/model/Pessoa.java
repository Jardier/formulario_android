package br.com.fic.android.formulario_android.model;

import java.util.ArrayList;

public class Pessoa {

	private String nome;
	private String telefone;
	
	private ArrayList<String> esportesFavoritos;
	private String tipoSanguineo;
	private String raca;

	public Pessoa() {
		esportesFavoritos = new ArrayList<String>();
	}

	public ArrayList<String> getEsportesFavoritos() {
		return esportesFavoritos;
	}

	public void setEsportesFavoritos(ArrayList<String> esportesFavoritos) {
		this.esportesFavoritos = esportesFavoritos;
	}

	public String getTipoSanguineo() {
		return tipoSanguineo;
	}

	public void setTipoSanguineo(String tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public void addEsportesFavoritos(String esporte) {
		getEsportesFavoritos().add(esporte);
	}

	public void removeEsportesFavoritos(String esporte) {
		getEsportesFavoritos().remove(esporte);
	}

	public String getRaca() {
		return raca;
	}
	
	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getEsportes() {
		String esportes = "";
		if(!this.esportesFavoritos.isEmpty()){
			for(int i = 0; i < this.esportesFavoritos.size(); i++) {
				esportes += this.esportesFavoritos.get(i) + ". ";
			}
		}
		
		return esportes;
	}
	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", telefone=" + telefone + ", esportesFavoritos=" + esportesFavoritos + ", tipoSanguineo=" + tipoSanguineo + ", raca=" + raca + "]";
	}
	
	
	
	
}
