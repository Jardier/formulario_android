package br.com.fic.android.formulario_android;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.fic.android.formulario_android.model.Pessoa;

public class MinhaListaAdapter extends ArrayAdapter<Pessoa> {
	private Context context;
	private List<Pessoa> pessoas;

	public MinhaListaAdapter(Context context, List<Pessoa> pessoas) {
		super(context, 0, pessoas);
		this.context = context;
		this.pessoas = pessoas;

	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Pessoa pessoa = pessoas.get(position);

		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.pessoa, null);
		}

		ImageView imageView = (ImageView) view.findViewById(R.id.imagemPessoa);
		imageView.setImageResource(R.drawable.pessoa);

		TextView nomeView = (TextView) view.findViewById(R.id.pessoaNome);
		nomeView.setText(context.getString(R.string.labelNome) + " " + pessoa.getNome());

		TextView telefoneView = (TextView) view.findViewById(R.id.pessoaTelefone);
		telefoneView.setText(context.getString(R.string.labelTelefone) + " " + pessoa.getTelefone());

		TextView esportesView = (TextView) view.findViewById(R.id.pessoaEsporte);
		esportesView.setText(context.getString(R.string.labelEsporte) + " " + pessoa.getEsportes());

		TextView tipoSanguineoView = (TextView) view.findViewById(R.id.pessoaTipoSanguineo);
		tipoSanguineoView.setText(context.getString(R.string.labelTipoSanguineo) + " " + pessoa.getTipoSanguineo());

		TextView racaView = (TextView) view.findViewById(R.id.pessoaRaca);
		racaView.setText(context.getString(R.string.labelRaca) + " " + pessoa.getRaca());

		return view;
	}
}
